# README #

### What is this repository for? ###

* GMnet is a Game maker studio 1.4/EA/2.0 network engine that use native gml.
* It use UDP Punch, UPnP to allow better connectivity.
* It have a LAN and online lobby. (if you setup the master server: https://the-any-key.itch.io/how-to-setup-a-gmnet-master-server-with-digitalocean)

### How do I get set up? ###

**Import your game to GMnet (Best method):**

1. Export your game to an extension.
2. Open GMnet base template in Game maker studio 1.4 or EA.
3. Import your game to the base template.

**Import GMnet to your game (We need to update the extension you export first):**

1. Open in Game maker studio 1.4 or EA.
2. Extensions>GMnet (double click)
3. Click "Export resources" tab
4. Click each resource in the right column (Extension:) and click the "Remove" button until every resource is gone.
5. Click "Add all"
6. Click"Ok".
7. Save the project.
8. Right click Extensions>GMnet>Export extension
9. Open your project and right click Extensions>Import extension
10. Import all

**Tutorials how to get started:**  
* https://www.youtube.com/watch?v=B2EG55iGuzo&list=PLfMGRgz7yTw-RtXnPV7a51aoZb3fufzZ9

### Who do I talk to? ###

* Discord: https://discord.gg/sFfhBsa
* Mail: support$theanykey.se